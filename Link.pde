class Link {
  
  Node n1, n2;
  float weight;
  
  Link(Node _n1, Node _n2, float _weight) {
    n1 = _n1;
    n2 = _n2;
    weight = _weight;
  }

  void strengthen(float dw) {
    weight += dw;
  }

  // All we're doing really is adding a display() function to a VerletParticle
  void display() {
    stroke(#3DB680);
    strokeWeight(0.2*log(weight+1.0));
    line(n1.pos.x*width, n1.pos.y*height, n2.pos.x*width, n2.pos.y*height);
  }
}

