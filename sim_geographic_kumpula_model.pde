// A list of cluster objects
Cluster cluster;

// Boolean that indicates whether we draw connections or not
boolean showParticles = true;
boolean showConnections = true;
boolean writing = false;
boolean dumpFrame = false;

// Font
PFont f;

float g_averageDegree = 0.0;
float g_CC = 0.0;
float g_averageWeight = 0.0;

void setup() {
  size(600, 600);
  f = createFont("Arial", 18, true);

  // Spawn a new graph
  cluster = new Cluster(300);
  
  frameRate(30);
}

void draw() {
  for( int i=0; i < 20; i++) {
    cluster.updateNetwork();
  }

  background(0);

  // Display all points
  if (showParticles) {
    cluster.showNodes();
  }

  // If we want to see the physics
  if (showConnections) {
    cluster.showConnections();
  }
  
  if( writing ) {
    PrintWriter writer = createWriter("net_proc.edg");
    cluster.Print(writer);
    writing = false;
  }

  // calculate Stylized facts
  if( frameCount % 9 == 0 ) {
    g_averageDegree = cluster.calcAverageDegree();
    g_CC = cluster.calcCC();
    g_averageWeight = cluster.calcAverageWeight();
  }

  // Print
  fill(255,120,255);
  textFont(f);
  String time = String.valueOf(frameCount/3);
  text("t = " + time + "\n<k> = " + g_averageDegree + "\nCC = " + g_CC + "\n<w> = " + g_averageWeight,10,20);

  if( dumpFrame && (frameCount % 3 == 0) ) {
    saveFrame("frames/####.tif");
  }
}

// Key press commands
void keyPressed() {
  if (key == 'c') {
    showConnections = showConnections ? false : true;
  }
  else if (key == 'p') {
    showParticles = showParticles ? false : true;
  }
  else if (key == 'o') {
    writing = true;
  }
}

