import java.util.Set;
import java.util.Map;
import java.util.Collection;

class Position {
  float x, y;
  
  Position(float _x, float _y) {
    x = _x;
    y = _y;
  }
  
  Position add(Position other) {
    return new Position( x + other.x, y + other.y );
  }
  
  Position mult(float d) {
    return new Position( x*d, y*d );
  }
  
  Position div(float d) {
    return new Position( x/d, y/d );
  }
  
  Position getCloseTo(Position other, float c) {
    return new Position( c*x + (1.0-c)*other.x, c*y + (1.0-c)*other.y);
  }
}

class Node {
  int id;
  Position pos;
  HashMap<Integer,Link> m_edges;

  Node(int _id, float x, float y) {
    id = _id;
    pos = new Position(x,y);
    m_edges = new HashMap<Integer,Link>();
  }

  void addEdge(Node node, Link link) {
    m_edges.put(node.id, link);
  }

  Collection<Link> allLinks() {
    return m_edges.values();
  }

  boolean hasEdge(Node node) {
    return ( m_edges.get(node.id) != null ) ? true : false;
  }

  Link getLinkTo(Node node) {
    return m_edges.get(node.id);
  }

  void deleteEdge(Node node) {
    m_edges.remove(node.id);
  }

  void clearEdge() {
    m_edges.clear();
  }

  int degree() {
    return m_edges.size();
  }
  
  float strength() {
    float sum = 0.0;
    for( Link l : m_edges.values() ) {
      sum += l.weight;
    }
    return sum;
  }

  Link edgeSelection(Node node) {
    if( node == null && degree() == 0 ) { return null; }
    if( node != null && degree() < 1 ) { return null; }

    float w_sum = 0.0;
    int id_to_skip = (node != null) ? node.id : -1;
    for( int nid : m_edges.keySet() ) {
      if( nid == id_to_skip ) { continue; }
      w_sum += m_edges.get(nid).weight;
    }
    float r = random(w_sum);
    Link ret = null;
    for( int nid : m_edges.keySet() ) {
      if( nid == id_to_skip ) { continue; }
      Link link = m_edges.get(nid);
      r -= link.weight;
      if( r <= 0.0 ) { ret = link; break; }
    }
    return ret;
  }

  float distanceTo(Node node) {
    float dx = node.pos.x - pos.x;
    float dy = node.pos.y - pos.y;
    return sqrt(dx*dx+dy*dy);
  }
  
  void moveRandomly() {
    pos.x = random(1.0);
    pos.y = random(1.0);
  }
  
  // All we're doing really is adding a display() function to a VerletParticle
  void display() {
    fill(#9D73BB,150);
    stroke(#9D73BB);
    strokeWeight(2);
    ellipse(pos.x*width,pos.y*height,4,4);
  }
}

