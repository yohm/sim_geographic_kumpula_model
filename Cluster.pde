class Cluster {
  // A cluster is a grouping of nodes
  ArrayList<Node> m_nodes;
  ArrayList<Link> m_links;
  int m_time_step;

  float p_init_weight = 1.0;
  float p_prob_la = 0.05;
  float p_prob_ga = 0.005;
  float p_alpha = 2.0;
  float p_prob_nd = 0.001;
  float p_noise = 0.01;
  float p_inertia = 0.99;

  // We initialize a Cluster with a number of nodes, a diameter, and centerpoint
  Cluster(int n) {

    // Initialize the ArrayList
    m_nodes = new ArrayList<Node>();
    m_links = new ArrayList<Link>();
    m_time_step = 0;

    // Create the nodes
    for (int i = 0; i < n; i++) {
      // We can't put them right on top of each other
      float x = random(1.0);
      float y = random(1.0);
      Node node = new Node(i, x, y);
      m_nodes.add(node);
    }
  }

  // Draw all nodes
  void showNodes() {
    for( Node n : m_nodes ) {
      n.display();
    }
  }

  // Draw all the internal connections
  void showConnections() {
    for( Link l: m_links ) {
      l.display();
    }
  }

  void Print(PrintWriter writer) {
    for( Link l : m_links ) {
      writer.println(String.valueOf(l.n1.id) + " " + String.valueOf(l.n2.id) + " " + String.valueOf(l.weight));
    }
  }

  Link addLink(Node ni, Node nj) {
    p_init_weight = 1.0;
    Link l = new Link(ni, nj, p_init_weight);
    m_links.add(l);
    ni.addEdge(nj, l);
    nj.addEdge(ni, l);
    return l;
  }

  Link addLink(int i, int j) {
    Node ni = m_nodes.get(i);
    Node nj = m_nodes.get(j);
    return addLink(ni, nj);
  }

  void strengthenLink(int link_idx) {
    Link l = m_links.get(link_idx);
    l.strengthen(1.0);
  }

  void removeLinksOfNode(Node node) {
    for( Link l : node.allLinks() ) {
      Node pair = ( l.n1.id == node.id ) ? l.n2 : l.n1;
      pair.deleteEdge(node);
      m_links.remove(l);
    }
    node.clearEdge();
  }

  void updateNetwork() {
    int num_nodes = m_nodes.size();

    int action = m_time_step % 3;
    for( Node node : m_nodes ) {
      if( action == 0 ) { LA(node);}
      else if( action == 1 ) { GA(node); }
      else if( action == 2 ) {
        ND(node);
        MoveNode(node);
      }
    }

    m_time_step += 1;
  }

  void LA(Node ni) {
    if( ni.degree() == 0 ) { return; }
    Link l_ij = ni.edgeSelection(null);
    if( l_ij == null ) { println("must not happen"); throw new RuntimeException("foo"); }
    l_ij.strengthen(1.0);

    Node nj = (l_ij.n1.id == ni.id) ? l_ij.n2 : l_ij.n1;
    if( nj.degree() == 1 ) { return; }
    Link l_jk = nj.edgeSelection(ni);
    if( l_jk == null ) { println("must not happen"); throw new RuntimeException("foo"); }
    l_jk.strengthen(1.0);

    Node nk = (l_jk.n1.id == nj.id) ? l_jk.n2 : l_jk.n1;
    Link l_ik = ni.getLinkTo(nk);
    if( l_ik == null ) {
      if( random(1.0) < p_prob_la ) {
        Link l = addLink(ni, nk);
      }
    }
    else {
      l_ik.strengthen(1.0);
    }
  }

  void GA(Node ni) {
    float epsilon = 0.0001;

    if( ni.degree() > 0 && random(1.0) > p_prob_ga ) { return; }

    float sum = 0.0;
    float[] probs = new float[m_nodes.size()];
    for( int j = 0; j < m_nodes.size(); j++) {
      if( j == ni.id ) { probs[j] = 0.0; continue; }
      Node nj = m_nodes.get(j);
      if( ni.hasEdge(nj) ) {
        probs[j] = 0.0;
      }
      else {
        float radius = ni.distanceTo(nj);
        if( radius <= epsilon ) { radius = epsilon; }
        float p = pow(radius, -p_alpha);
        probs[j] = p;
        sum += p;
      }
    }
    float r = random(sum);
    for( int j = 0; j < m_nodes.size(); j++) {
      r -= probs[j];
      if( r < 0.0 ) {
        Node nj = m_nodes.get(j);
        if( ni.hasEdge(nj) ) { throw new RuntimeException("must not happen"); }
        Link l = addLink(ni, nj);
        return;
      }
    }

    throw new RuntimeException("must not happen");
  }

  void ND(Node ni) {
    if( random(1.0) > p_prob_nd ) { return; }

    removeLinksOfNode(ni);
    ni.moveRandomly();
  }
  
  void MoveNode(Node ni) {
    if( ni.degree() == 0 ) return;
    Position cm = new Position( 0.0, 0.0 );
    for( Link l : ni.allLinks() ) {
      Node nj = (l.n1.id == ni.id) ? l.n2 : l.n1;
      cm = cm.add( nj.pos.mult(l.weight) );
    }
    cm = cm.div( ni.strength() );
    ni.pos = ni.pos.getCloseTo(cm, p_inertia).add( new Position(p_noise*randomGaussian(), p_noise*randomGaussian()) );
  }

  float calcAverageDegree() {
    return 2.0 * float(m_links.size()) / m_nodes.size();
  }

  float calcCC() {
    float sum = 0.0;
    for( Node n : m_nodes ) {
      sum += calcLocalCC(n);
    }
    return sum / m_nodes.size();
  }
  float calcLocalCC(Node n) {
    int k = n.degree();
    if( k <= 1 ) { return 0.0; }
    float connected = 0.0;
    Set<Integer> neighbors = n.m_edges.keySet();
    for( int i : neighbors ) {
      for( int j : neighbors ) {
        if( i < j ) continue;
        connected += m_nodes.get(i).hasEdge(m_nodes.get(j)) ? 1.0 : 0.0;
      }
    }
    float localCC = connected * 2.0 / (k*(k-1));
    return localCC;
  }

  float calcAverageWeight() {
    float sum = 0.0;
    for( Link l : m_links ) { sum += l.weight; }
    return sum / m_links.size();
  }
}

