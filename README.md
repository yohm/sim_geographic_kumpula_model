## visualizing Weighted Social Network model with mobile agents

- First of all, you need to install [processing](http://processing.org/).
- Open `sim_geographic_kumpula_model.pde` in processing, and run it.

## License

All of the source code is licensed under the MIT License.

